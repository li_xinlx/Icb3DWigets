/**
 * Cesium自带控件以及各种图层的加载，图层也可以通过index去展示
 * 具体详情查阅官方文档
 */
var viewer = new Cesium.Viewer("cesiumContainer", {
    animation: true,//左下角时间动画罗盘
    timeline: true,//正下方时间线
    selectionIndicator: false,
    baseLayerPicker: true,//图层选择器
    fullscreenButton: true,//全屏按钮
    vrButton: true,//是否开启VR模式
    geocoder: true,//查找控件
    homeButton: true,//home控件
    sceneModePicker: true,//选择视角的模式
    creditContainer: null,
    infoBox: true,
    navigationHelpButton: true,//帮助控件
    imageryProvider: new Cesium.UrlTemplateImageryProvider({//加载高德地图图层
        // url:"//www.google.cn/maps/vt?lyrs=s,h&gl=CN&x={x}&y={y}&z={z}",
        url: "http://webrd0{s}.is.autonavi.com/appmaptile?lang=zh_cn&size=1&scale=1&style=8&x={x}&y={y}&z={z}",
        subdomains: ["1", "2", "3", "4"]
    }),
    imageryProvider: new Cesium.UrlTemplateImageryProvider({
        url: "//www.google.cn/maps/vt?lyrs=s,h&gl=CN&x={x}&y={y}&z={z}",//加载谷歌地图图层
        //   url:  "http://webrd0{s}.is.autonavi.com/appmaptile?lang=zh_cn&size=1&scale=1&style=8&x={x}&y={y}&z={z}",
        subdomains: ["1", "2", "3", "4"]
    }),
    terrainProvider: new Cesium.CesiumTerrainProvider({//加载中国地理信息高度图层
        url: '//lab.earthsdk.com/terrain/577fd5b0ac1f11e99dbd8fd044883638',
        requestVertexNormals: true,
        requestWaterMask: true
    })
});

viewer.cesiumWidget.creditContainer.style.display = "none"

loadzh();